import React from "react";
import { createRoot, extend } from "@react-three/fiber";
import * as THREE from "three"

extend(THREE)


self.onmessage = ({data}) => {
  const {canvas} = data;
  // Not setting canvas.style also causes an error if size is set
  // canvas.style = {};
  const root = createRoot(canvas);
  root.configure({
    dpr: 1,
    // Setting a size causes it to skip the computeInitialSize call which errors
    // size: {
    //   width: canvas.width,
    //   height: canvas.height,
    //   top: 0,
    //   left: 0,
    // },
  });

  root.render(
    <>
      <ambientLight intensity={0.5} />
        <mesh>
          <sphereGeometry />
          <meshStandardMaterial />
        </mesh>
    </>
  );
  
}
