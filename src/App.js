import logo from './logo.svg';
import './App.css';
import { Canvas, extend } from '@react-three/fiber';
import { useEffect, useRef } from 'react';
import * as THREE from "three"

extend(THREE)


function App() {
  const canvas = useRef();
  const isTransferred = useRef(false);

  useEffect(() => {
    const worker = new Worker(new URL('./simple.worker.js', import.meta.url))
    if(canvas.current && !isTransferred.current) {
      const offscreen = canvas.current.transferControlToOffscreen();
      worker.postMessage({canvas: offscreen}, [offscreen])
      isTransferred.current = true;
    }
  }, [])

  return (
    <div className="App">
      <canvas ref={canvas} />
    </div>
  );
}

export default App;
